extends KinematicBody2D


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
export var speed = 400.0
export var gravity = 1200.0
export var jump_speed = -600.0
var jump_counter = 0
var velocity = Vector2()
const UP = Vector2(0,-1)
var sprite_delay = 0

func get_input():
	velocity.x = 0
	# if stationary, reset condition
	if is_on_floor():
		jump_counter = 0
		$Sprite.frame = 0
	# if right is pressed, move right and animate
	if Input.is_action_pressed("right"):
		velocity.x += speed
		if $Sprite.get_frame() == 0:
			$Sprite.frame = 10
		sprite_delay += 1
		if $Sprite.get_frame() == 10 and sprite_delay >= 10:
			$Sprite.frame = 9
			sprite_delay += 1
			if sprite_delay >= 20:
				sprite_delay = 0
		$Sprite.flip_h = false
	# if left is pressed, move left, flip sprite horizontally and animate
	if Input.is_action_pressed("left"):
		velocity.x -= speed
		if $Sprite.get_frame() == 0:
			$Sprite.frame = 10
		sprite_delay += 1
		if $Sprite.get_frame() == 10 and sprite_delay >= 10:
			$Sprite.frame = 9
			sprite_delay += 1
			if sprite_delay >= 20:
				sprite_delay = 0
		$Sprite.flip_h = true
	# if has jumped less than two times and pressed space, jump
	if Input.is_action_just_pressed("ui_select") and jump_counter < 2:
		velocity.y = jump_speed
		jump_counter += 1
		$Sprite.frame = 1
	# if dash button is pressed, then dash
	# the conditions are different based on if moving left or right
	if Input.is_action_pressed("dash") and is_on_floor():
		if $Sprite.flip_h:
			velocity.x -= 300
		if !$Sprite.flip_h:
			velocity.x += 300
	# if dash button is released, stop dashing
	# as with dashing, conditions are based on moving left or right
	if Input.is_action_just_released("dash") and is_on_floor():
		if $Sprite.flip_h:
			velocity.x += 300
		if !$Sprite.flip_h:
			velocity.x -= 300
	# if player not jumping and stopped moving, reset 
	if Input.is_action_just_released("left") or Input.is_action_just_released("right"):
		if $Sprite.get_frame() != 1:
			$Sprite.frame = 0
		
func _physics_process(delta):
	velocity.y += delta * gravity
	get_input()
	velocity = move_and_slide(velocity, UP)
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
